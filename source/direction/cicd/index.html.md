---
layout: markdown_page
title: "Product Vision - CI/CD"
---

- TOC
{:toc}

## CI/CD at GitLab

The CI/CD sub-department focuses on code verification and release management. CI/CD serves
as the critical bridge between the creation of digital artifacts and their availability in 
production environments. As the nexus of the DevOps lifecycle, CI/CD is critical broader adoption
of DevOps within an organization.

### North Stars
GitLab follows the following North Stars within CI/CD:

1. **Speed** - A slow CI/CD pipeline can handicap an organization's time-to-value. Businesses are increasingly under
pressure to ship new software faster. GitLab focuses on enabling a blazing fast CI/CD pipeline.
1. **Automation** - The days of spending months preparing CI/CD pipelines before developers can start utilizing them are over. GitLab
will provide you ready to use pipelines out of the box and ensure you can spend more time focused on shipping code and less time 
optimizing your pipelines.
1. **Security & Compliance** - In business critical applications, there cannot be a choice between speed of iteration and security. 
GitLab will continue to add compliance and security capabilities to your pipeline.

### Workflows

The following workflows are the primary focus of the CI/CD group:
- Pipeline Setup: Teams creating new projects should be able to quickly and easily 
- Pipeline Verification: Team managers should be able to verify definitely if their team's pipelines meet requirements
- Release Management: Release Managers, and Business Leaders should be able to review and manually advance releases
- CI/CD Health Check: Team Directors should be able to review and optimize their pipelines

## Stages

The CI/CD group contains three major product stages:

- [Verify](/direction/verify): Build and test your software
- [Package](/direction/package): Bundle your built code for delivery
- [Release](/direction/release): Deliver your software to your users

You can see how these relate to each other in the following infographic:

![Pipeline Infographic](https://about.gitlab.com/images/blogimages/cicd_pipeline_infograph.png "Pipeline Infographic")
