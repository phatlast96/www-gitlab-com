---
layout: markdown_page
title: "Finance"
---

## Welcome to the GitLab Finance Handbook
{: .no_toc}

The GitLab Finance team includes multiple functional groups: Accounting, Financial Planning & Analysis, Legal, Tax and Business Operations Programs.
{: .note}

----

## On this page
{:.no_toc}

- TOC
{:toc .toc-list-icons}

----

## <i class="fab fa-gitlab fa-fw icon-color font-awesome" aria-hidden="true"></i> Quick Links

[Finance Issues](https://gitlab.com/gitlab-com/finance/issues){:.btn .btn-purple-inv}
- Please use confidential issues for topics that should only be visible to team members at GitLab.

[Contract and Payment Approval Process](/handbook/finance/procure-to-pay){:.btn .btn-purple-inv}

[Operating Metrics](/handbook/finance/operating-metrics/){:.btn .btn-purple-inv}

[Sales Comp Plan](/handbook/finance/accounting/#sales-compensation-plan){:.btn .btn-purple-inv}

[Signature Authorization Matrix](/handbook/finance/authorization-matrix){:.btn .btn-purple-inv}

[Stock Options](/handbook/stock-options/){:.btn .btn-purple-inv}

[Travel and Expense Guidelines](/handbook/finance/accounting/#travel-and-expense-guidelines){:.btn .btn-purple-inv}

## Forms

[Link to W9 Doc](https://drive.google.com/file/d/1dQbQtYav_QZ1M00RnxMgs8ciM68ZlYrm/view){:.btn .btn-purple-inv}

[Link to form CA-590](https://drive.google.com/a/gitlab.com/file/d/0BzE3Rq8kSQ6Tcmp3a19xcFBZOWs/view?usp=sharing){:.btn .btn-purple-inv}

## Correspondence 

- Please use the `#finance` chat channel in Slack for questions that don't seem appropriate for the issue tracker or internal email correspondence.

- **Accounts Payable**- inquiries which relate to vendor and merchant invoices should be sent to our Accounts Payable mailbox - ap@gitlab.com. Of course, electronic copies of all invoices should be sent to this address upon receipt.

- **Accounts Receivable**- customer billing inquiries should be sent to our Accounts Receivable mailbox – ar@gitlab.com

- **Payroll**- inquiries which relate to contractor invoices should be sent to our Payroll mailbox - payroll@gitlab.com.

----

<div class="alert alert-purple center"><h3 class="purple"><strong>We <i class="fas fa-heart orange font-awesome" aria-hidden="true"></i> Finance</strong></h3></div>

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Finance Handbooks
{: #finance-handbooks}

- [Accounting](/handbook/finance/accounting/)
- [Financial Planning & Analysis](/handbook/finance/financial-planning-and-analysis/)
- [Legal](/handbook/legal/)
- [Tax](/handook/tax/)
- [Business Operations](/handbook/business-ops/)

----

## General Topics

### Legal and Financial Information

For commonly requested company information, please visit our [wiki page](https://gitlab.com/gitlab-com/finance/wikis/company-information).

### Company Accounts
<a name="company-accounts"></a>

Login information for the following accounts can be found in the Secretarial vault
on 1Password:

- FedEx
- Amazon
- IND (Immigratie en Naturalisatie Dienst, in the Netherlands) company number

If you need this information but cannot find it in your shared vaults, check with PeopleOps to get access.

## Invoice template and where to send
<a name="invoices"></a>

Vendor invoices are to be sent to ap@gitlab.com and payroll@gitlab.com for contractors. An [invoice
template](https://docs.google.com/spreadsheets/d/1sRA2uCpFblOleyVIslqM4YwbW27GkU5DTgwMLhgR_Iw/edit?usp=sharing) can be found in Google Docs by the name of "Invoice Template".

Contractors can send the invoice around the middle of the month to ensure payment by the end of it. Include any [expenses](/handbook/spending-company-money/) in the invoice.

In many cases, VAT will not be payable on transactions between GitLab BV and EU-based
vendors/contractors, thanks to "Shifted VAT". To make use of this shifted VAT:

* The vendor/contractor writes the phrase "VAT shifted to recipient according to
article 44 and 196 of the European VAT Directive" on the invoice along with the
VAT-number of GitLab BV (NL853740343B01).
* On the vendor's VAT return the revenue from GitLab BV goes to the rubric "Revenue within the EU". It goes without saying that vendors are responsible for their own correct and timely filings.
* GitLab BV files the VAT on the VAT return, and is generally able to deduct this VAT all as part of the same return.

### Timesheets for Hourly Employees

1. People Ops and Finance will share a private Google Sheet with you where you will log your hours for each day in the “hours” column.
1. There is a dropdown in the “pay type” column, with the default being Regular. There are also options for Overtime, Vacation, Sick, and Bereavement. Choose the appropriate pay type for your time.
1. If you work overtime or more hours than agreed upon in your contract, please obtain approval from your manager and forward to Finance before payroll cutoff.
1. Your timesheet is due one day prior to the submit payroll date, which is outlined for the calendar year on your timesheet.

<!-- EXTRA STYLES APPLIED FOR THIS PAGE ONLY -->

<style>
.purple {
  color: rgb(107,79,187) !important;
}
.orange {
  color:rgb(252,109,38) !important;
}
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
.btn-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-purple:hover {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv:hover {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-orange {
  color: rgb(252,109,38);
  background-color: #fff;
  border-color: rgb(226,67,41);
}
.btn-orange:hover {
  color: #fff;
  background-color: rgb(252,109,38);
  border-color: rgb(226,67,41);
}
.product.thumbnail img {
  display: block;
  max-width: 50%;
  margin: 20px auto;
}
.thumbnail img {
  display: block;
  max-width: 30%;
  margin: 20px auto;
}
.caption h4 {
  text-align: center;
}
.mkt-box {
  padding-bottom: 10px;
  padding-top: 10px;
  cursor: pointer;
}
.mkt-box:hover {
  /*border-radius: 5px;*/
  box-shadow:0 1px 5px rgba(0,0,0,0.3), 0 0 2px rgba(0,0,0,0.1) inset;
}
.mkt-row {
  padding-top: 20px;
  padding-bottom: 5px;
}
.mkt-row a:focus {
  outline: none;
}
.modal-header h2 {
  margin-top: 0;
}
.modal-footer p {
  margin-bottom: 0;
}
.center {
  text-align: center;
  display: block;
  margin-right: auto;
  margin-left: auto;
}
.description {
  color: #999;
}
.extra-space {
  margin-bottom: 5px;
}
.alert-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: rgba(107,79,187,.5);
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 25px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
ul.toc-list-icons li ul li i.slack {
  color: rgb(224,23,101);
}
ul.toc-list-icons li ul li i.email {
  color: rgb(192,0,0);
}
</style>

