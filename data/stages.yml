stages:

  # Stages are defined in the product categories handbook https://about.gitlab.com/handbook/product/categories/#hierarchy

  manage:
    display_name: "Manage"
    marketing: true
    image: "/images/solutions/solutions-manage.png"
    description: "Gain visibility and insight into how your business is performing."
    body: |
          GitLab helps teams manage and optimize their software delivery lifecycle with metrics and value stream insight in order to streamline and increase their delivery velocity.   Learn more about how GitLab helps to manage your end to end <a href="https://about.gitlab.com/vsm">value stream.</a>
    vision: /direction/manage
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amanage&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    related:
      - "plan"
      - "create"
      - "verify"
      - "package"
      - "release"
      - "configure"
      - "monitor"
      - "secure"
      - "defend"
    dept: dev
    groups:
      control:
        name: Control
        pm: Jeremy Watson
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang (Interim)
        test_automation_engineers:
          - Sanad Liaquat
        ux:
          - Chris Peressini
        tech_writer: Evan Read
        categories:
          - audit_management
          - authentication_and_authorization
          - workflow_policies
      framework:
        name: Framework
        pm: Jeremy Watson
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang (Interim)
        test_automation_engineers:
          - Sanad Liaquat
        ux:
          - Chris Peressini
        tech_writer: Evan Read
        categories:
          - cycle_analytics
          - devops_score
          - code_analytics

  plan:
    display_name: "Plan"
    marketing: true
    image: "/images/solutions/solutions-plan.png"
    description: "Regardless of your process, GitLab provides powerful planning
      tools to keep everyone synchronized."
    body: |
      GitLab enables portfolio planning and management through epics, groups (programs) and milestones to organize and track progress.  Regardless of your methodology from Waterfall to DevOps, GitLab’s simple and flexible approach to planning meets the needs of small teams to large enterprises.

      GitLab helps teams organize, plan, align and track project work to ensure teams are working on the right things at the right time and maintain end to end visibility and traceability of issues throughout the delivery lifecycle from idea to production.
    vision: /direction/plan
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Aplan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2011
    related:
      - "create"
    dept: dev
    groups:
      team_planning:
        name: Team Planning
        pm: Victor Wu
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Sean McGivern
        frontend_engineering_manager: André Luís
        test_automation_engineers:
          - Ramya Authappan
        ux:
          - Annabel Dunstone Gray
          - Alexis Ginsberg
        tech_writer: Mike Lewis
        categories:
          - project_management
          - kanban_boards
          - time_tracking
      enterprise_planning:
        name: Enterprise Planning
        pm: Victor Wu
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Sean McGivern
        frontend_engineering_manager: André Luís
        test_automation_engineers:
          - Ramya Authappan
        ux:
          - Annabel Dunstone Gray
          - Alexis Ginsberg
        tech_writer: Mike Lewis
        categories:
          - agile_portfolio_management
          - value_stream_management
      certify:
        name: Certify
        pm: Victor Wu
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Sean McGivern
        frontend_engineering_manager: André Luís
        test_automation_engineers:
          - Ramya Authappan
        ux:
          - Annabel Dunstone Gray
          - Alexis Ginsberg
        tech_writer: Mike Lewis
        categories:
          - requirements_management
          - quality_management
          - service_desk

  create:
    display_name: "Create"
    marketing: true
    image: "/images/solutions/solutions-create.png"
    description: |
      Create, view, and manage code and project data through powerful branching
      tools.
    body: |
      GitLab helps teams design, develop and securely manage code and project
      data from a single distributed version control system to enable rapid
      iteration and delivery of business value.  GitLab repositories provide a
      scalable, single source of truth for collaborating on projects and code
      which enables teams to be productive without disrupting their workflows.
    vision: /direction/create
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Acreate&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2011
    related:
      - "plan"
      - "verify"
    dept: dev
    groups:
      source_code:
        name: Source Code
        pm: James Ramsay
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Douwe Maan
        frontend_engineering_manager: André Luís
        test_automation_engineers:
          - Mark Lapierre
          - Tomislav Nikić
        ux:
          - Pedro Moreira da Silva
          - M.V.
        tech_writer: Marcia Ramos
        categories:
          - source_code_management
          - code_review
      knowledge:
        name: Knowledge
        pm: James Ramsay
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Douwe Maan
        frontend_engineering_manager: André Luís
        test_automation_engineers:
          - Mark Lapierre
          - Tomislav Nikić
        ux:
          - Pedro Moreira da Silva
          - M.V.
        tech_writer: Marcia Ramos
        categories:
          - design_management
          - wiki
      editor:
        name: Editor
        pm: James Ramsay
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Douwe Maan
        frontend_engineering_manager: André Luís
        test_automation_engineers:
          - Mark Lapierre
          - Tomislav Nikić
        ux:
          - Pedro Moreira da Silva
          - M.V.
        tech_writer: Marcia Ramos
        categories:
          - web_ide
          - live_coding
          - search
          - snippets
      gitaly:
        name: Gitaly
        image: "/images/solutions/solutions-gitaly.png"
        description: |
          Gitaly is a Git RPC service for handling all the git calls made by
          GitLab.
        body: |
          GitLab makes Gitaly
        vision: "https://gitlab.com/groups/gitlab-org/-/epics/710"
        backend_engineering_manager: Tommy (interim)
        frontend_engineering_manager: Tim Z (Interim)
        ux:
          - M.V.
        tech_writer: Axil
        internal_customers:
          - Quality Department
          - Infrastructure Department
        categories:
          - gitaly
      gitter:
        name: Gitter
        body: |
          Gitter is an open source instant messaging application for
          developers, and is the place to connect the open source and software
          development community.
        vision: "https://gitlab.com/groups/gitlab-org/-/epics/850"
        backend_engineering_manager: Tommy (interim)
        frontend_engineering_manager: Tim Z (Interim)
        ux:
          - M.V.
        tech_writer: Axil
        categories:
          - gitter

  verify:
    display_name: "Verify"
    marketing: true
    image: "/images/solutions/solutions-verify.png"
    description: "Keep strict quality standards for production code with automatic testing and reporting."
    body: |
      GitLab helps delivery teams fully embrace continuous integration to automate the builds, integration and verification of their code.  GitLab’s industry leading CI capabilities enables automated testing, Static Analysis Security Testing, Dynamic Analysis Security testing and code quality analysis to provide fast feedback to developers and testers about the quality of their code.  With pipelines that enable concurrent testing and parallel execution, teams quickly get insight about every commit, allowing them to deliver higher quality code faster.
    vision: /direction/verify
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Averify&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2012
    related:
      - "create"
    dept: cicd
    groups:
      ci:
        name: "CI & Runner"
        pm: "Brendan O'Leary"
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Elliot Rushton
        frontend_engineering_manager: Tim Z (Interim)
        ux:
          - Dimitrie Hoekstra
        tech_writer: Evan Read
        internal_customers:
          - Quality Department
          - UX Department
          - Infrastructure Department
        categories:
          - continuous_integration
      testing:
        name: "Testing & Other"
        pm: "Brendan O'Leary (interim)"
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Elliot Rushton
        frontend_engineering_manager: Tim Z (Interim)
        ux:
          - Dimitrie Hoekstra
        tech_writer: Evan Read
        internal_customers:
          - Quality Department
          - UX Department
          - Infrastructure Department
        categories:
          - code_quality
          - performance_testing
          - system_testing
          - usability_testing
          - accessibility_testing
          - compatibility_testing

  package:
    display_name: "Package"
    marketing: true
    image: "/images/solutions/solutions-package.png"
    description: "Create a consistent and dependable software supply chain with built-in universal package management."
    body: |
      GitLab enables teams to package their applications and dependancies, manage containers, and build artifacts with ease. The private, secure container registry and artifact repositories are built-in and preconfigured out-of-the box to work seamlessly with GitLab source code management and CI/CD pipelines. Ensure DevOps acceleration with automated software pipelines that flow freely without interuption.
    vision: /direction/package
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Apackage&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    dept: cicd
    groups:
      package:
        name: Package
        pm: Jason Lenny (interim)
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Marin Jankovski (Interim)
        frontend_engineering_manager: Clement Ho
        ux:
          - Amelia Bauerly
          - Sarah Voegeli
        tech_writer: Axil
        internal_customers:
          - Distribution Team
        categories:
          - container_registry
          - maven_repository
          - npm_registry
          - rubygem_registry
          - linux_package_registry
          - helm_chart_registry
          - dependency_proxy

  secure:
    display_name: "Secure"
    marketing: true
    image: "/images/solutions/solutions-secure.png"
    description: "Security capabilities, integrated into your development lifecycle."
    body: |
      GitLab provides Static Application Security Testing (SAST), Dynamic Application Security Testing (DAST), Container Scanning, and Dependency Scanning to help you deliver secure applications.
    vision: /direction/secure
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Asecure&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2017
    related:
      - "create"
      - "defend"
    dept: secure
    groups:
      static_analysis:
        name: Static Analysis
        pm: Fabio Busatto
        pmm: Cindy Blake
        cm: Erica Lindberg
        backend_engineering_manager: Philippe Lafoucrière
        frontend_engineering_manager: Lukas Eipert (Interim)
        ux:
          - Andy Volpe
          - Tim Noah
          - Kyle Mann
        tech_writer: Axil
        internal_customers:
          - Security Department
        categories:
          - static_application_security_testing
          - secret_detection
          - language_specific
      dynamic_analysis:
        name: Dynamic Analysis
        pm: Fabio Busatto
        pmm: Cindy Blake
        cm: Erica Lindberg
        backend_engineering_manager: Philippe Lafoucrière
        frontend_engineering_manager: Lukas Eipert (Interim)
        ux:
          - Andy Volpe
          - Tim Noah
          - Kyle Mann
        tech_writer: Axil
        internal_customers:
          - Security Department
        categories:
          - dynamic_application_security_testing
          - interactive_application_security_testing
          - fuzzing
      sca:
        name: Software Composition Analysis
        pm: Fabio Busatto
        pmm: Cindy Blake
        cm: Erica Lindberg
        backend_engineering_manager: Philippe Lafoucrière
        frontend_engineering_manager: Lukas Eipert (Interim)
        ux:
          - Andy Volpe
          - Tim Noah
          - Kyle Mann
        tech_writer: Axil
        internal_customers:
          - Security Department
        categories:
          - dependency_scanning
          - container_scanning
          - license_management
          - vulnerability_database

  release:
    display_name: "Release"
    marketing: true
    image: "/images/solutions/solutions-release.png"
    description: "GitLab's integrated CD solution allows you to ship code with zero-touch, be it on one or one thousand servers."
    body: |
      GitLab helps automate the release and delivery of applications, shortening the delivery lifecycle, streamlining manual processes, and accelerating team velocity. With zero-touch Continuous Delivery (CD) built right into the pipeline, deployments can be automated to multiple environments like staging and production, and the system just knows what to do without being told - even for more advanced patterns like canary deployments. With feature flags, built-in auditing/traceability, on-demand environments, and GitLab pages for static content delivery, you'll be able to deliver faster and with more confidence than ever before.
    vision: /direction/release
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Arelease&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    related:
      - "configure"
    dept: cicd
    groups:
      release_core:
        name: "Core Release"
        pm: Jason Lenny
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Darby Frey
        frontend_engineering_manager: John Hampton
        ux:
          - Rayana Verissimo
        tech_writer: Marcia Ramos
        internal_customers:
          - Delivery Team
          - Distribution Team
          - Gitter
          - Security Department
          - Tech Writers
        categories:
          - continuous_delivery
          - incremental_rollout
          - release_orchestration
          - release_governance
      release_support:
        name: "Supporting Capabilities"
        pm: Jason Lenny (interim)
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Darby Frey
        frontend_engineering_manager: John Hampton
        ux:
          - Rayana Verissimo
        tech_writer: Marcia Ramos
        internal_customers:
          - Distribution Team
          - Gitter
          - Security Department
          - Tech Writers
        categories:
          - pages
          - review_apps
          - feature_flags

  configure:
    display_name: "Configure"
    marketing: true
    image: "/images/solutions/solutions-configure.png"
    description: "Configure your applications and infrastructure."
    body: "GitLab helps teams to configure and manage their application environments.  Strong integrations to Kubernetes simplifies the effort to define and configure the infrastructure needed to support your application.   Protect access to key infrastructure configuration details such as passwords and login information by using ‘secret variables’ to limit access to only authorized users and processes."
    vision: /direction/configure
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Aconfigure&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2018
    related:
      - "configure"
    dept: ops
    groups:
      auto_devops:
        name: "Auto DevOps & Kubernetes"
        pm: Daniel Gruesso
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Dylan Griffith
        frontend_engineering_manager: Tim Z (Interim)
        test_automation_engineers:
          - Dan Davison
        ux:
          - Taurie Davis
        tech_writer: Evan Read
        internal_customers:
          - Quality Department
          - Site Availability Engineering
          - Site Reliability Engineering
        categories:
          - auto_devops
          - kubernetes_configuration
          - chatops
          - runbook_configuration
          - chaos_engineering
          - cluster_cost_optimization
      serverless:
        name: "Serverless & PaaS"
        pm: Daniel Gruesso (interim)
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Dylan Griffith
        frontend_engineering_manager: Tim Z (Interim)
        test_automation_engineers:
          - Dan Davison
        ux:
          - Taurie Davis
        tech_writer: Evan Read
        internal_customers:
          - Quality Department
          - Site Availability Engineering
          - Site Reliability Engineering
        categories:
          - serverless
          - paas

  monitor:
    display_name: "Monitor"
    marketing: true
    image: "/images/solutions/solutions-monitor.png"
    description: "Automatically monitor metrics so you know how any change in code
      impacts your production environment."
    body: |
      You need feedback on what the effect of your release is in order to do
      release management. Get monitoring built-in, see the code change
      right next to the impact that it has so you can respond quicker and
      effectively. No need to babysit deployment to manually get feedback.
      Automatically detect buggy code and prevent it from affecting the
      majority of your users.
    vision: /direction/monitor
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amonitor&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    related:
      - "release"
    dept: ops
    groups:
      apm:
        name: APM
        pm: Joshua Lambert (interim)
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Seth Engelhard
        frontend_engineering_manager: Clement Ho
        ux:
          - Amelia Bauerly
          - Sarah Voegeli
        tech_writer: Axil
        internal_customers:
          - Infrastructure Department
        categories:
          - metrics
          - tracing
          - logging
      debugging_health:
        name: "Debugging and Health"
        pm: Joshua Lambert
        pmm: William Chia
        cm: Aricka Flowers
        backend_engineering_manager: Seth Engelhard
        frontend_engineering_manager: Clement Ho
        ux:
          - Amelia Bauerly
          - Sarah Voegeli
        tech_writer: Axil
        internal_customers:
          - Infrastructure Department
        categories:
          - error_tracking
          - cluster_monitoring
          - synthetic_monitoring
          - incident_management
          - status_page

  defend:
    display_name: "Defend"
    marketing: true
    image: "/images/stages-devops-lifecycle/placeholder.png"
    description: "Defend your apps and infrastructure from security intrusions."
    body: |
      GitLab provides runtime application security, threat detection and management, data security, and application infrastructure security.
    vision: /direction/defend
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Adefend&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2019
    related:
      - "release"
      - "secure"
      - "manage"
    dept: defend
    groups:
      runtime_application_security:
        name: Runtime Application Security
        pm: Fabio Busatto
        pmm: Cindy Blake
        cm: Erica Lindberg
        backend_engineering_manager: Philippe Lafoucrière
        frontend_engineering_manager: Lukas Eipert (Interim)
        ux:
          - Andy Volpe
          - Tim Noah
          - Kyle Mann
        tech_writer: Axil
        internal_customers:
          - Security Department
        categories:
          - runtime_application_self_protection
          - web_firewall
      threat_management:
        name: "Threat Management"
        pm: Fabio Busatto
        pmm: Cindy Blake
        cm: Erica Lindberg
        backend_engineering_manager: Philippe Lafoucrière
        frontend_engineering_manager: Lukas Eipert (Interim)
        ux:
          - Andy Volpe
          - Tim Noah
          - Kyle Mann
        tech_writer: Axil
        internal_customers:
          - Security Department
        categories:
          - threat_detection
          - behavior_analytics
          - vulnerability_management
      application_infrastructure_security:
        name: Application Infrastructure Security
        pm: Fabio Busatto
        pmm: Cindy Blake
        cm: Erica Lindberg
        backend_engineering_manager: Philippe Lafoucrière
        frontend_engineering_manager: Lukas Eipert (Interim)
        ux:
          - Andy Volpe
          - Tim Noah
          - Kyle Mann
        tech_writer: Axil
        internal_customers:
          - Security Department
        categories:
          - data_loss_prevention
          - storage_security
          - container_network_security

  growth:
    display_name: Growth
    dept: growth
    related:
      - manage
      - create
      - verify
      - package
      - configure
      - monitor
      - secure
      - defend
    established: 2019
    groups:
      activation:
        name: Activation
        description: "Acquired and activated users, last 30 days"
        pm: Eric Brinkman (interim)
        engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang (Interim)
      adoption:
        name: Adoption
        description: "Adoption of stage cross-use - Stage Monthly Active Users (SMAU)"
        pm: Eric Brinkman (interim)
        engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang (Interim)
      upsell:
        name: Upsell
        description: "Conversion and Upsell - % of users who went to a (higher) subscription"
        pm: Eric Brinkman (interim)
        engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang (Interim)
      retention:
        name: Retention
        description: "Retention including reactivation"
        pm: Eric Brinkman (interim)
        engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang (Interim)

  enablement:
    display_name: Enablement
    dept: enablement
    related:
      - "create"
    established: 2012
    groups:
      distribution:
        name: Distribution
        image: "/images/solutions/solutions-distribution.png"
        description: "Add distribution description here"
        body: |
          GitLab helps distribution
        vision: /direction/distribution
        roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Adistribution&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
        pm: Joshua Lambert (interim)
        pmm: William Chia
        backend_engineering_manager: Marin Jankovski
        frontend_engineering_manager: Clement Ho
        tech_writer: Axil
        categories:
          - omnibus
          - cloud_native_installation
        internal_customers:
          - Quality Department
          - Infrastructure Department
      fulfillment:
        name: Fulfillment
        pm: Jeremy Watson
        pmm: John Jeremiah
        cm: Suri Patel
        backend_engineering_manager: Liam McAndrew
        frontend_engineering_manager: Dennis Tang (Interim)
        tech_writer: Evan Read
        categories:
          - licensing
          - transactions
          - telemetry
      geo:
        name: "Geo"
        image: "/images/solutions/solutions-geo.png"
        description: "Add Geo description here"
        body: |
          GitLab makes Geo
        vision: /direction/geo
        roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Adistribution&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
        pm: Andreas Kämmerle
        pmm: John Jeremiah
        backend_engineering_manager: Rachel Nienaber
        frontend_engineering_manager: André Luís (Interim)
        tech_writer: Evan Read
        categories:
          - geo
      memory:
        name: Memory
        description: "Responsible for optimizing GitLab application performance by reducing and optimizing memory resources required."
      ecosystem:
        name: Ecosystem
        description: "Supporting the success of third-party products integrating with GitLab."
        categories:
          - service_integrations
